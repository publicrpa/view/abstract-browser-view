package publicrpa.view;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Collectors;

public class Screenshot {

    private static final int QUEUE_SIZE = 10;
	private static final Queue<Path> screenQueue = new PriorityQueue<>(QUEUE_SIZE);

    public static void add(Path screenShot) {
        if (screenQueue.size() == QUEUE_SIZE) {
            screenQueue.remove();
        }
        screenQueue.add(screenShot);
    }

    public static List<Path> getQueue() {
        return screenQueue
        			.stream()
        			.sorted(Collections.reverseOrder())
        			.collect(Collectors.toList());
    }
}
