package publicrpa.view;

import static java.time.Duration.ofSeconds;
import static java.time.Duration.ofMillis;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.frameToBeAvailableAndSwitchToIt;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractBrowserView {

	private static final String filePattern = "seleniumimage-";
	public static WebDriver driver;

    protected void waitForFrameToBeAvailable(WebElement element) {
        new WebDriverWait(driver, ofSeconds(10)).until(frameToBeAvailableAndSwitchToIt(element));
    }

    protected WebElement waitForPresenceOfElementLocated(By locator) {
        new WebDriverWait(driver, ofSeconds(20), ofSeconds(20)).until(presenceOfElementLocated(locator));
        return driver.findElement(locator);
    }

    protected WebElement waitForVisibilityOfElementLocated(By locator) {
        return waitForVisibilityOfElementLocated(locator, 5);
    }

    protected WebElement waitForVisibilityOfElementLocated(By locator, long timeOutInSeconds) {
        return new WebDriverWait(driver, ofSeconds(timeOutInSeconds), ofMillis(100)).until(visibilityOfElementLocated(locator));
    }

    protected WebElement waitForVisibilityOf(WebElement element, long timeOutInSeconds) {
        return new WebDriverWait(driver, ofSeconds(timeOutInSeconds), ofMillis(100)).until(visibilityOf(element));
    }

    protected WebElement waitForClickabilityOf(WebElement element, long timeOutInSeconds) {
        return new WebDriverWait(driver, ofSeconds(timeOutInSeconds), ofMillis(100)).until(elementToBeClickable(element));
    }

    protected void waitForTextInElementLocated(By locator, String text) {
        new WebDriverWait(driver, ofSeconds(20), ofMillis(20)).until(d -> d.findElement(locator).getText().contains(text));
    }

    protected void waitForTextInActiveElement(String infix) {
        new WebDriverWait(driver, ofSeconds(20), ofMillis(20)).until(d -> d.switchTo().activeElement().getText().contains(infix));
    }

    protected WebElement waitForLowerCaseTextInOneOfmanyElementsLocated(By locator, String text) {
        new WebDriverWait(driver, ofSeconds(80), ofMillis(20)).until(d -> d.findElements(locator).stream().filter(e -> e.getText().trim().toLowerCase().contains(text)).findAny().isPresent());
        return driver.findElements(locator).stream().filter(e -> e.getText().trim().toLowerCase().contains(text)).findAny().get();
    }

	protected String makeScreenShot() {
		Path path = Paths.get(LocalDateTime.now().toString().replace(":", "-") + ".png");
		byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		try {
			Files.write(path, screenshot);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return path.toString();
	}

	public String takeScreenshotFromFlow() {
		Path path = Paths.get(".", filePattern + System.currentTimeMillis() + ".png");
		byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		try {
			Files.write(path, screenshot);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Screenshot.add(path.toAbsolutePath());
		return path.toAbsolutePath().toString();
	}

	protected void miscrosoftSSO(String login, String password) {
		waitForVisibilityOfElementLocated(By.id("i0116")).sendKeys(login);
		driver.findElement(By.id("idSIButton9")).click();

		waitForVisibilityOfElementLocated(By.id("passwordInput")).sendKeys(password);
		driver.findElement(By.id("submitButton")).click();

		waitForVisibilityOfElementLocated(By.id("idSIButton9"), 30).click();
	}

}
